from mimetypes import init
from msilib.schema import Class
from Empleado import Empleado
from Proyecto import Proyecto

class AsignacionProyecto(Empleado, Proyecto):
    def __init__(self, nombre, apellido, salario,nombreP,horas):
        #Empleado.__init__(nombre, apellido, salario)
        #Proyecto.__init__(nombreP)
        self.horas = horas
        self.nombre =nombre
        self.apellido = apellido
        self.salario =salario
        self.nombreP = nombreP

    def imprimir_informacion(self):
        print(f"""\x1b[1;37m
         Ficha Proyecto
    \x1b[1;31m-------------------------\x1b[1;37m
    - Nombre      : \x1b[1;34m{self.nombre}\x1b[1;37m
    - Apellido    : \x1b[1;34m{self.apellido}\x1b[1;37m 
    - Proyecto    : \x1b[1;35m{self.nombreP}\x1b[1;37m
    - Horas Asig. : \x1b[1;33m{self.horas}\x1b[1;37m \n""")