from Empleado import Empleado
from Gerente import Gerente
from Desarrollador import Desarrollador
from Proyecto import Proyecto
from AsignacionProyecto import AsignacionProyecto


PruebaPoo = AsignacionProyecto("Kristofer","Abella",1000,"Prueba_POO", 2)
PruebaPoo.imprimir_informacion()

EmpleadoPrueba = Empleado("Kristofer", "Abella", 1000)
EmpleadoPrueba.imprimir_informacion()

DesarrolladorPrueba = Desarrollador("Kristofer", "Abella", 1000, "Python")
DesarrolladorPrueba.imprimir_informacion()

GerentePrueba = Gerente("Kristofer", "Abella", 1000, "TI")
GerentePrueba.imprimir_informacion()

ProyectoPrueba = Proyecto("Prueba_POO")
ProyectoPrueba.imprimir_nombre()

Empleado1 = Empleado("Yamil", "Briceño", 2001)
Empleado2 = Empleado("Jaime", "Jerez", 1020)
Empleado3 = Empleado("Margarett", "Aravena", 1300)

Empleados=[Empleado1,Empleado2,Empleado3]

def suma_sueldo(Empleados):
    total = 0
    for e in Empleados:
        total += e.salario
    print(f"- Total de sueldo es : \x1b[1;31m{total}\x1b[1;37m\n")

suma_sueldo(Empleados)


def imprimir_txt(PruebaPOO):
    import os
    file = open("C:/Users/sistemas/Desktop/Prueba_POO_PC/asignacion.txt","w")
    file.write(f"{PruebaPoo.nombre}" + os.linesep)
    file.write(f"{PruebaPoo.apellido}" + os.linesep)
    file.write(f"{PruebaPoo.nombreP}" + os.linesep)
    file.write(f"{PruebaPoo.horas}" + os.linesep)
    file.close

imprimir_txt(PruebaPoo)


