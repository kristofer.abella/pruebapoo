from Empleado import Empleado

class Gerente(Empleado):
    def __init__(self, nombre, apellido, salario,area):
        super().__init__(nombre, apellido, salario)
        self.area = area

    def imprimir_informacion(self):
        print(f"""\x1b[1;37m
          Ficha Gerente
    \x1b[1;31m-------------------------\x1b[1;37m
    - Nombre   : \x1b[1;34m{self.nombre}\x1b[1;37m
    - Apellido : \x1b[1;32m{self.apellido}\x1b[1;37m 
    - Salario  : \x1b[1;35m{self.salario}\x1b[1;37m
    - Area     : \x1b[1;35m{self.area}\x1b[1;37m \n""")