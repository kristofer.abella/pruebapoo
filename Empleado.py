class   Empleado:
    def __init__(self,nombre,apellido,salario):
        self.nombre = nombre
        self.apellido = apellido
        self.salario = salario

    def imprimir_informacion(self):
        print(f"""\x1b[1;37m
         Ficha Empleado
    \x1b[1;31m------------------------\x1b[1;37m
    - Nombre   : \x1b[1;34m{self.nombre}\x1b[1;37m
    - Apellido : \x1b[1;32m{self.apellido}\x1b[1;37m 
    - Salario  : \x1b[1;35m{self.salario}\x1b[1;37m \n""")